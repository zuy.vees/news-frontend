import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import SearchList from "../components/SearchList";

const SearchPage = () => {
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar />

      <SearchList />

      <Footer />
    </div>
  );
};

export default SearchPage;
