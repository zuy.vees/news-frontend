import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorMancanegara, setcolorMancanegara] = useState("orange");
  const [categoryMancanegara, setcategoryMancanegara] = useState(7);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorMancanegara={colorMancanegara} />
      <CardNews1 categoryMancanegara={categoryMancanegara} />
      <div className="news-content">
        <HighlightNews categoryMancanegara={categoryMancanegara} />
        <NewsList categoryMancanegara={categoryMancanegara} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryMancanegara={categoryMancanegara} />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
