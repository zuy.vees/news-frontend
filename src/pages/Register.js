import React from "react";
import { Link } from "react-router-dom";
import {
  UserOutlined,
  MailOutlined,
  PhoneOutlined,
  LockOutlined,
} from "@ant-design/icons";
import { Button, Form, Input, Radio } from "antd";
import "../App.css";

function Register() {
  const [form] = Form.useForm();

  const handleSubmit = (values) => {
    console.log("Register:", values);
  };

  return (
    <div className="container">
      <div className="form-box">
        <h2 style={{ textAlign: "center" }}>Register</h2>
        <Form form={form} onFinish={handleSubmit}>
          <Form.Item
            name="registrationType"
            rules={[{ required: true, message: "Pilih daftar sebagai apa!" }]}
          >
            <Radio.Group>
              <Radio value="user">User</Radio>
              <Radio value="author">Author</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Username harus diisi!" }]}
          >
            <Input prefix={<UserOutlined />} placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Email harus diisi!" },
              { type: "email", message: "Email tidak valid!" },
            ]}
          >
            <Input prefix={<MailOutlined />} placeholder="Email" />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            rules={[
              { required: true, message: "Nomor telepon harus diisi!" },
              {
                pattern: /^[0-9]*$/,
                message: "Gunakan angka untuk nomor telepon!",
              },
            ]}
          >
            <Input prefix={<PhoneOutlined />} placeholder="Phone Number" />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: "Password harus diisi!" }]}
          >
            <Input.Password prefix={<LockOutlined />} placeholder="Password" />
          </Form.Item>
          <Form.Item
            name="confirmPassword"
            dependencies={["password"]}
            hasFeedback
            rules={[
              { required: true, message: "Konfirmasi password harus diisi!" },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject("Password konfirmasi tidak cocok!");
                },
              }),
            ]}
          >
            <Input.Password
              prefix={<LockOutlined />}
              placeholder="Confirm Password"
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Register
            </Button>
          </Form.Item>
        </Form>
        <p>
          Sudah punya akun? <Link to="/login">Login di sini</Link>
        </p>
      </div>
    </div>
  );
}

export default Register;
