import React, { useState } from "react";
import { Button, Form, Input } from "antd";
import axios from "axios";

import "../App.css";

const AdminPage = (...res) => {
  const { TextArea } = Input;

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [detail, setDetail] = useState("");
  const [image, setImage] = useState(null);
  const [recommendation, SetRecommendation] = useState("");
  const [category, setCategory] = useState("");
  const [user, setUser] = useState("");

  const onSubmit = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("title", title);
    formData.append("description", description);
    formData.append("detail", detail);
    formData.append("image", image);
    formData.append("recommendation", recommendation);
    formData.append("category_id", category);
    formData.append("user_id", user);
    console.log(formData);
    await axios
      .post("https://news-backend-lovat.vercel.app/api/news", formData, {
        headers: { "content-type": "multipart/form-data" },
      })
      .then((res) => {
        console.log("post success: ", res);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };

  const onImageUpload = (e) => {
    const file = e.target.files[0];
    setImage(file);
  };

  return (
    <div className="admin-container">
      <div className="form-box1">
        <h2 style={{ textAlign: "center" }}>Admin Page</h2>
        <Form>
          <Form.Item
            name="title"
            className="title"
            rules={[{ required: true, message: "title harus diisi!" }]}
          >
            <Input
              className="input-title"
              placeholder="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </Form.Item>
          <Form.Item
            name="description"
            rules={[{ required: true, message: "Description harus diisi!" }]}
          >
            <Input
              className="input-description"
              placeholder="Description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Item>
          <Form.Item
            name="detail"
            rules={[{ required: true, message: "Detail news harus diisi!" }]}
          >
            <TextArea
              rows={10}
              className="input-detail"
              placeholder="Detail News"
              value={detail}
              onChange={(e) => setDetail(e.target.value)}
            />
          </Form.Item>

          <Form.Item
            name="image"
            rules={[{ required: true, message: "Image harus diisi!" }]}
          >
            <Input
              type="file"
              name="image"
              accept="image/*"
              placeholder="image"
              onChange={(e) => onImageUpload(e)}
            />
          </Form.Item>
          <Form.Item
            name="recommendation"
            rules={[{ required: true, message: "Recommendation harus diisi!" }]}
          >
            <Input
              className="input-recommendation"
              placeholder="Recommendation (true/false)"
              value={recommendation}
              onChange={(e) => SetRecommendation(e.target.value)}
            />
          </Form.Item>
          <Form.Item
            name="category"
            rules={[{ required: true, message: "Category harus diisi!" }]}
          >
            <Input
              className="input-category"
              placeholder="Category ID"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            />
          </Form.Item>
          <Form.Item
            name="user"
            rules={[{ required: true, message: "User harus diisi!" }]}
          >
            <Input
              className="user"
              placeholder="User ID"
              value={user}
              onChange={(e) => setUser(e.target.value)}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block onClick={onSubmit}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default AdminPage;
