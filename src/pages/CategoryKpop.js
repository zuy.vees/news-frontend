import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorKpop, setcolorKpop] = useState("orange");
  const [categoryKpop, setcategoryKpop] = useState(5);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorKpop={colorKpop} />
      <CardNews1 categoryKpop={categoryKpop} />
      <div className="news-content">
        <HighlightNews categoryKpop={categoryKpop} />
        <NewsList categoryKpop={categoryKpop} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryKpop={categoryKpop} />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
