import React, { useState } from "react";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import "../index.css";
import CardNews1 from "../components/CardNews1";
import HighlightNews from "../components/HighlightNews";
import NewsList from "../components/NewsList";
import CardNews2 from "../components/CardNews2";

const CategoryPolitik = () => {
  const [colorOlahraga, setcolorOlahraga] = useState("orange");
  const [categoryOlahraga, setcategoryOlahraga] = useState(2);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Navbar colorOlahraga={colorOlahraga} />
      <CardNews1 categoryOlahraga={categoryOlahraga} />
      <div className="news-content">
        <HighlightNews categoryOlahraga={categoryOlahraga} />
        <NewsList categoryOlahraga={categoryOlahraga} />
      </div>
      <div className="news-title-recommendation">
        <h2>Rekomendasi Berita</h2>
        <CardNews2 categoryOlahraga={categoryOlahraga} />
      </div>
      <Footer />
    </div>
  );
};

export default CategoryPolitik;
