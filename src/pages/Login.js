import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Layout, Image, Button, Form, Input } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import axios from "axios";
// import useAuth from "hooks/useAuth";
import "../App.css";

function Login() {
  const [form] = Form.useForm();
  const navigation = useNavigate();

  const handleSubmit = async (values) => {
    console.log(`${process.env.REACT_APP_SITE_PROD}/auth/login`);
   await axios
    .post(`${process.env.REACT_APP_SITE_PROD}/auth/login`, values)
    .then((res) => {
      console.log(res.data.accessToken);
      localStorage.setItem("token" ,JSON.stringify(res.data.accessToken))
      // localStorage.removeItem('token');
      // setToken(res.data.accessToken);
      navigation("/", { replace: true });
    })
    .catch((err) => {
      // console.log(err);
      // setError(err.response.data.error);
      // setSnackType("error");
    });
  };


  return (
    <div className="container">
      <div className="form-box">
        <h2 style={{ textAlign: "center" }}>Login</h2>
        <Form form={form} onFinish={handleSubmit}>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Username!",
              },
            ]}
          >
            <Input prefix={<UserOutlined />} placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input.Password prefix={<LockOutlined />} placeholder="Password" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Login
            </Button>
          </Form.Item>
        </Form>
        <p>
        Don't have an account? <Link to="/register">Register Here</Link>
        </p>
      </div>
    </div>
  );
}

export default Login;
