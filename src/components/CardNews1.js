import React from "react";
import { useFetch } from "../hooks/useFetch";

const CardNews1 = (props) => {
  const { data } = useFetch("https://news-frontend-lovat.vercel.app/api/news");
  const categoryHome = props.categoryHome;
  const categoryPolitik = props.categoryPolitik;
  const categoryOlahraga = props.categoryOlahraga;
  const categoryEkonomi = props.categoryEkonomi;
  const categoryEntertainment = props.categoryEntertainment;
  const categoryKpop = props.categoryKpop;
  const categoryBudaya = props.categoryBudaya;
  const categoryMancanegara = props.categoryMancanegara;
  return (
    <div>
      {data && (
        <section id="late-news" className="late-news">
          {categoryHome != 0
            ? data.slice(1, 5).map((item) => {
                if (
                  item.category_id ===
                  (categoryHome ||
                    categoryPolitik ||
                    categoryOlahraga ||
                    categoryEkonomi ||
                    categoryEntertainment ||
                    categoryKpop ||
                    categoryBudaya ||
                    categoryMancanegara)
                ) {
                  return (
                    <div className="late-news-container">
                      <img
                        class="news-image"
                        src={item.image}
                        width="200"
                        height="100"
                        alt="News Image"
                      />
                      <h5>{item.title}</h5>
                    </div>
                  );
                }
              })
            : data.slice(1, 5).map((item) => {
                {
                  return (
                    <div className="late-news-container">
                      <img
                        class="news-image"
                        src={item.image}
                        width="200"
                        height="100"
                        alt="News Image"
                      />
                      <h5>{item.title}</h5>
                    </div>
                  );
                }
              })}
        </section>
      )}
    </div>
  );
};

export default CardNews1;
