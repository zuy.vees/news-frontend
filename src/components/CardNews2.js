import React, { useState } from "react";
import { useFetch } from "../hooks/useFetch";

const CardNews2 = (props) => {
  const { data } = useFetch("https://news-backend-lovat.vercel.app/api/news");
  const categoryHome = props.categoryHome;
  const categoryPolitik = props.categoryPolitik;
  const categoryOlahraga = props.categoryOlahraga;
  const categoryEkonomi = props.categoryEkonomi;
  const categoryEntertainment = props.categoryEntertainment;
  const categoryKpop = props.categoryKpop;
  const categoryBudaya = props.categoryBudaya;
  const categoryMancanegara = props.categoryMancanegara;
  return (
    <div>
      {data && (
        <div className="news-content-recommendation">
          {categoryHome != 0
            ? data.slice(5, 13).map((item) => {
                if (
                  (item.category_id ===
                    (categoryPolitik ||
                      categoryOlahraga ||
                      categoryEkonomi ||
                      categoryEntertainment ||
                      categoryKpop ||
                      categoryBudaya ||
                      categoryMancanegara)) &
                  (item.recommendation === true)
                ) {
                  return (
                    <div key={item.news_id} className="card-news">
                      <img
                        className="news-image"
                        src={item.image}
                        width="300"
                        height="150"
                        alt="News Image"
                      />
                      <p>{item.title}</p>
                    </div>
                  );
                }
              })
            : data.slice(5, 13).map((item) => {
                {
                  return (
                    <div key={item.news_id} className="card-news">
                      <img
                        className="news-image"
                        src={item.image}
                        width="300"
                        height="150"
                        alt="News Image"
                      />
                      <p>{item.title}</p>
                    </div>
                  );
                }
              })}
        </div>
      )}
    </div>
  );
};

export default CardNews2;