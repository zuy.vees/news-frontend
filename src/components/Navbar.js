import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Input, Space } from "antd";
import { useFetch } from "../hooks/useFetch";

const Navbar = (props) => {
  const { Search } = Input;
  const colorPolitik = props.colorPolitik;
  const colorOlahraga = props.colorOlahraga;
  const colorEkonomi = props.colorEkonomi;
  const colorEntertainment = props.colorEntertainment;
  const colorKpop = props.colorKpop;
  const colorBudaya = props.colorBudaya;
  const colorMancanegara = props.colorMancanegara;

  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(
        `https://news-frontend-steel.vercel.app/api/news/search/${searchTerm}`
      );
      if (response.ok) {
        const data = await response.json();
        setSearchResults(data);
      }
    } catch (error) {
      console.error("Error searching:", error);
    }
  };

  return (
    <div className="navbar">
      <section id="header-navbar">
        <div className="header">
          <div>
            <Link to={`/`}>
              <img
                className="logo"
                src="https://res.cloudinary.com/dcz5fjpsa/image/upload/v1696488684/eqp0bgnayyoofep6alow.jpg"
                width={200}
                height={50}
                alt="logo"
              />
            </Link>
          </div>
          <div className="search-bar">
            <Space>
              <Search
                placeholder="Search"
                enterButton
                onClick={handleSearch}
                value={searchTerm}
                style={{
                  width: 500,
                }}
                href="/search-list"
                onChange={(e) => setSearchTerm(e.target.value)}
              />
            </Space>
            <ul>
              {searchResults &&
                searchResults.map((result) => <li>{result.title}</li>)}
            </ul>
          </div>
          <ul>
            <button className="btn-login">
              <Link to={`/login`}>Login</Link>
            </button>
          </ul>
        </div>

        <div className="topnav">
          <nav>
            <Link to={`/politik`}>
              <h2 style={{ color: colorPolitik }}>Politik</h2>
            </Link>
            <Link to={`/olahraga`}>
              <h2 style={{ color: colorOlahraga }}>Olahraga</h2>
            </Link>
            <Link to={`/ekonomi`}>
              <h2 style={{ color: colorEkonomi }}>Ekonomi</h2>
            </Link>
            <Link to={`/entertainment`}>
              <h2 style={{ color: colorEntertainment }}>Entertaiment</h2>
            </Link>
            <Link to={`/kpop`}>
              <h2 style={{ color: colorKpop }}>Kpop</h2>
            </Link>
            <Link to={`/budaya`}>
              <h2 style={{ color: colorBudaya }}>Budaya</h2>
            </Link>
            <Link to={`/mancanegara`}>
              <h2 style={{ color: colorMancanegara }}>Manca Negara</h2>
            </Link>
          </nav>
        </div>
      </section>
    </div>
  );
};

export default Navbar;
