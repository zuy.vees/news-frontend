import React, { useFetch } from "react";

const ListSearch = () => {
  const { data } = useFetch("https://news-backend-lovat.vercel.app/api/news");

  return <ul>{"" && "".map((item) => <li>{item.title}</li>)}</ul>;
};

export default ListSearch;
