import React from "react";
import { useFetch } from "../hooks/useFetch";

const HighlightNews = (props) => {
  const { data } = useFetch("https://news-backend-lovat.vercel.app/api/news");
  console.log(data);
  const categoryHome = props.categoryHome;
  const categoryPolitik = props.categoryPolitik;
  const categoryOlahraga = props.categoryOlahraga;
  const categoryEkonomi = props.categoryEkonomi;
  const categoryEntertainment = props.categoryEntertainment;
  const categoryKpop = props.categoryKpop;
  const categoryBudaya = props.categoryBudaya;
  const categoryMancanegara = props.categoryMancanegara;
  return (
    <div>
      {data && (
        <div>
          {categoryHome != 0
            ? data.slice(0, 1).map((item) => {
                if (
                  item.category_id ===
                  (categoryPolitik ||
                    categoryOlahraga ||
                    categoryEkonomi ||
                    categoryEntertainment ||
                    categoryKpop ||
                    categoryBudaya ||
                    categoryMancanegara)
                ) {
                  return (
                    <div className="news-content-primer">
                      <img
                        class="news-image"
                        src={item.image}
                        width="700"
                        height="400"
                        alt="News Image"
                      />
                      <h2 class="title-text-news-content">{item.title}</h2>
                    </div>
                  );
                }
              })
            : data.slice(0, 1).map((item) => {
                {
                  return (
                    <div className="news-content-primer">
                      <img
                        class="news-image"
                        src={item.image}
                        width="700"
                        height="400"
                        alt="News Image"
                      />
                      <h2 class="title-text-news-content">{item.title}</h2>
                    </div>
                  );
                }
              })}
        </div>
      )}
    </div>
  );
};

export default HighlightNews;
