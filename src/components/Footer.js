const Footer = () => {
  return (
    <div className="footer">
      <section id="footer">
        <h2>Copyright @ 2023 | Group 2 FSW-35</h2>
        <br />
      </section>
    </div>
  );
};

export default Footer;
