import React from "react";
import { useFetch } from "../hooks/useFetch";

const NewsList = (props) => {
  const { data } = useFetch("https://news-backend-lovat.vercel.app/api/news");
  const categoryHome = props.categoryHome;
  const categoryPolitik = props.categoryPolitik;
  const categoryOlahraga = props.categoryOlahraga;
  const categoryEkonomi = props.categoryEkonomi;
  const categoryEntertainment = props.categoryEntertainment;
  const categoryKpop = props.categoryKpop;
  const categoryBudaya = props.categoryBudaya;
  const categoryMancanegara = props.categoryMancanegara;
  return (
    <div>
      {data && (
        <section id="sidebar">
          <div class="sidebar-content">
            <h2>Berita Populer</h2>
            {categoryHome != 0
              ? data.map((item) => {
                  if (
                    (item.category_id ===
                      (categoryPolitik ||
                        categoryOlahraga ||
                        categoryEkonomi ||
                        categoryEntertainment ||
                        categoryKpop ||
                        categoryBudaya ||
                        categoryMancanegara)) &
                    (item.view_number > 0)
                  ) {
                    return (
                      <div className="sidebar-item">
                        <ul>
                          <li hrfe="">{item.title}</li>
                        </ul>
                      </div>
                    );
                  }
                })
              : data.map((item) => {
                  if (item.view_number > 0) {
                    return (
                      <div className="sidebar-item">
                        <ul>
                          <li hrfe="">{item.title}</li>
                        </ul>
                      </div>
                    );
                  }
                })}

            <div class="banner">
              <img
                class="banner-image"
                href="#"
                src="https://www.flashfly.net/wp/wp-content/uploads/2022/06/iPhone-15-Pro-concept-render-4RMD-1.jpg"
                width="300"
                height="150"
                alt="News Image"
              />
            </div>
          </div>
        </section>
      )}
    </div>
  );
};
export default NewsList;
