import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import AdminPage from "./pages/AdminPage";
import CategoryPolitik from "./pages/CategoryPolitik";
import CategoryEkonomi from "./pages/CategoryEkonomi";
import CategoryEntertainment from "./pages/CategoryEntertainment";
import CategoryKpop from "./pages/CategoryKpop";
import CategoryOlahraga from "./pages/CategoryOlahraga";
import CategoryBudaya from "./pages/CategoryBudaya";
import CategoryMancanegara from "./pages/CategoryMancanegara";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/admin-page" element={<AdminPage />} />
          <Route path="/politik" element={<CategoryPolitik />} />
          <Route path="/ekonomi" element={<CategoryEkonomi />} />
          <Route path="/entertainment" element={<CategoryEntertainment />} />
          <Route path="/kpop" element={<CategoryKpop />} />
          <Route path="/olahraga" element={<CategoryOlahraga />} />
          <Route path="/budaya" element={<CategoryBudaya />} />
          <Route path="/mancanegara" element={<CategoryMancanegara />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
